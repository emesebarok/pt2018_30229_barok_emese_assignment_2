package controller;

import frame.*;
import model.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Simulator implements Runnable {

	public static final int SLEEP_TIME_UNIT = 100;
	
	int simulationInterval = 100; 
	int minServiceTime = 1;
	int maxServiceTime = 5;
	int minArrivalInterval = 1;
	int maxArrivalInterval = 5;
	int maxNumberOfSchedulers = 3;
	private ArrayList<Scheduler> schedulers;
	private Frame frame;
	AtomicInteger currentTime = new AtomicInteger(0);

	/**
	 * Instantiates a simulator object with a single scheduler, it also displays the initial user interface waiting for input.
	 */
	public Simulator() {
		schedulers = new ArrayList<Scheduler>();
		schedulers.add(new Scheduler());
		frame = new Frame();
		frame.setStartButtonListener(new StartButtonListener());
	}

	/** 
	 * Contains the code which is executed as a stand alone thread.
	 * It contains two loops the first one adds new tasks to the queues
	 * and opens new queues if necessary until it reaches the time limit of the simulation.
	 * The second loop continues the iterations until all the queues become empty then terminates the schedulers. 
	 * At the end the final statistics are displayed.
	 */
	public void run() {
		int nextArrivalTime = 1;
		while (getCurrentTime() < simulationInterval) {
			currentTime.incrementAndGet();	
			frame.setCurrentTime(getCurrentTime());
			
			boolean newScheduler = false;
			for (Scheduler scheduler : schedulers) {
				scheduler.setCurrentTime(getCurrentTime());
				if(scheduler.getServer().isOverloaded()) newScheduler = true;
			}
			
			if (newScheduler && schedulers.size() < maxNumberOfSchedulers) {
				schedulers.add(new Scheduler());
				balanceSchedulers();
			}
			
			if (getCurrentTime() == nextArrivalTime) {
				Scheduler shortest = schedulers.get(getMinWaitingTimeSchedulerIndex());
				int serviceTime = (int) (Math.random() * (maxServiceTime - minServiceTime) + minServiceTime);
				
				Task t = new Task(getCurrentTime(), serviceTime, getMinWaitingTimeSchedulerIndex());
				System.out.println(t.toString());
				shortest.dispatchTaskOnServer(t);
				
				nextArrivalTime = (int)(Math.random() * (maxArrivalInterval - minArrivalInterval) + getCurrentTime() + minArrivalInterval);
			}
			
			frame.displayData(schedulers);
			
			try {
				Thread.sleep(Simulator.SLEEP_TIME_UNIT);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
		
		//at this point currentTime > simualtionTime
		//we need to serve the customers who are already in the queues
		boolean existsNonEmptyScheduler = true;
		while (existsNonEmptyScheduler) {
			currentTime.incrementAndGet();
			
			existsNonEmptyScheduler = false;
			for (Scheduler scheduler : schedulers) {
				scheduler.setCurrentTime(getCurrentTime());
				if(!scheduler.getServer().isEmpty() || scheduler.getServer().getCurrentTask() != null) existsNonEmptyScheduler = true;
			}
			
			
			frame.setCurrentTime(getCurrentTime());
			frame.displayData(schedulers);
			
			try {
				Thread.sleep(Simulator.SLEEP_TIME_UNIT);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		Statistics.setSimulationFinishTime(getCurrentTime());
		Statistics.displayTaskQueue();
		System.out.println("\nFinal Statistics\n\nMin Waiting Time = " + Statistics.getMinWaitingTime() 
						 + " ; \nMax Waiting Time = " + Statistics.getMaxWaitingTime() 
						 + " ; \nAvg Waiting Time = " + Statistics.getAverageWaitingTime()
						 + " ; \nPeak Hour Start Time =  " + Statistics.getPeakHourStartingTime()
						 + " ; \nPeak Hour Task Number = " + Statistics.getPeakHourTaskNumber());
		
		frame.displayFinalStatistics();
		
		for (Scheduler scheduler : schedulers) {
			scheduler.terminate();
		}
	}
	
	//Gets the value of the current simulator time.
	public int getCurrentTime() {
		return currentTime.get();
	}
	
	/**
	 * After opening a new queue the tasks who are the last ones in their respective queues move to the new empty queue so that the
	 * tasks will be evenly distributed between all the queues.
	 */
	public void balanceSchedulers() {
		int totalSize = 0;
		for (Scheduler scheduler : schedulers) totalSize+=scheduler.getServer().getSize();
		int newSize = totalSize/schedulers.size();
		System.out.println("New size: " + newSize);
		for(int i = 0; i < schedulers.size() - 1; i++) {
			Task[] tasks = schedulers.get(i).getServer().getTasks();
			int j = 1;
			while (schedulers.get(i).getServer().getSize() > newSize) {
				System.out.println("Removing: " + j + "    " + tasks[tasks.length-j].toString());
				tasks[tasks.length-j].setQ(schedulers.size()-1);
				schedulers.get(schedulers.size()-1).dispatchTaskOnServer(tasks[tasks.length-j]);
				schedulers.get(i).getServer().removeTask(tasks[tasks.length-j]);
				j++;				
			}
		}
	}
	
	//Walks through the list of schedulers and gets the index of the shortest one (the one with minimal waiting time).
	public int getMinWaitingTimeSchedulerIndex() {
		int minWaitingTime = schedulers.get(0).getServer().getWaitingTime();
		int index = 0;
		for (Scheduler scheduler : schedulers) {
			if (scheduler.getServer().getWaitingTime() < minWaitingTime) {
				index = schedulers.indexOf(scheduler);
			}
		}
		return index;
	}
	
	//Validates a given input String. Verifies if it represents a positive non-zero integer.
	public boolean validateInput(String input) {
		if (input.equals("")) return true;
		return input.matches("^[1-9]\\d*"); // one or more digits, cannot start with 0
	}
	
	
	private class StartButtonListener implements ActionListener {		
		
		public void actionPerformed(ActionEvent arg0) {
			if (validateInput(frame.getSimInterval())) {
				simulationInterval = Integer.valueOf(frame.getSimInterval());
			}
			else {
				frame.displayErrorMessage("Simulation Interval");
			}
			
			if (validateInput(frame.getMaxNumberOfSchedulers())) {
				maxNumberOfSchedulers = Integer.valueOf(frame.getMaxNumberOfSchedulers());
			}
			else {
				frame.displayErrorMessage("Max Number of Queues");
			}
			
			if (validateInput(frame.getMaxArrivalInterval())) {
				maxArrivalInterval = Integer.valueOf(frame.getMaxArrivalInterval());
			}
			else {
				frame.displayErrorMessage("Max Arrival Interval");
			}
			
			if (validateInput(frame.getMinArrivalInterval())) {
				minArrivalInterval = Integer.valueOf(frame.getMinArrivalInterval());
			}
			else {
				frame.displayErrorMessage("Min Arrival Interval");
			}
			
			if (validateInput(frame.getMinServiceTime())) {
				minServiceTime = Integer.valueOf(frame.getMinServiceTime());
			}
			else {
				frame.displayErrorMessage("Min Service Time");
			}
			
			if (validateInput(frame.getMaxServiceTime())) {
				maxServiceTime = Integer.valueOf(frame.getMaxServiceTime());
			}
			else {
				frame.displayErrorMessage("Max Service Time");
			}
			
			Thread th = new Thread(Simulator.this);
			th.start();
			
		}
		
	}

}
