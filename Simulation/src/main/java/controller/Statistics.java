package controller;

import model.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Statistics {
	public static BlockingQueue<Task> taskQueue = new LinkedBlockingQueue<>();
	public static int simulationFinishTime = 0;
	
	public static void displayTaskQueue() {
		System.out.println("Logger contents: ");
		for(Task t : taskQueue) {
			System.out.println(t.toString());
		}
	}
	
	/**
	 * Based on the list of finished tasks it returns the starting minute of the peak hour.
	 * Peak hour means the hour during which the most tasks are processed.
	 */
	public static int getPeakHourStartingTime() {
		int peakHour = 0, nrOfClients = 0;
		Task[] task = new Task[taskQueue.size()];
		taskQueue.toArray(task);
		for (int i = 0; i < simulationFinishTime; i+= 60) {
			int j = 0, k = 0;
			while (task[j].getArrivalTime() < i && j < task.length-1) j++;
			while (task[j].getFinishTime() < i + 60 && j < task.length-1) {
				k++; j++;
			}
			if (k > nrOfClients) {
				nrOfClients = k;
				peakHour = i;
			}
		}
		return peakHour;
	}
	
	/**
	 * Based on the list of finished tasks it returns the number of tasks processed during the peak hour.
	 * Peak hour means the hour during which the most tasks are processed.
	 */
	public static int getPeakHourTaskNumber() {
		int peakHour = 0, nrOfClients = 0;
		Task[] task = new Task[taskQueue.size()];
		taskQueue.toArray(task);
		for (int i = 0; i < simulationFinishTime; i+= 60) {
			int j = 0, k = 0;
			while (task[j].getArrivalTime() < i && j < task.length-1) j++;
			while (task[j].getFinishTime() < i + 60 && j < task.length-1) {
				k++; j++;
			}
			if (k > nrOfClients) {
				nrOfClients = k;
				peakHour = i;
			}
		}
		return nrOfClients;
	}
	
	/**
	 * Calculates the average waiting time for all the tasks.
	 */
	public static float getAverageWaitingTime() {
		float waitingTime = 0;
		for(Task t : taskQueue) {
			waitingTime+=(t.getFinishTime() - t.getArrivalTime());
		}
		return waitingTime / taskQueue.size();
	}
	
	/**
	 * Calculates the max waiting time for all the tasks.
	 */
	public static float getMaxWaitingTime() {
		int waitingTime = 0;
		for(Task t : taskQueue) {
			int tmp = t.getFinishTime() - t.getArrivalTime();
			if (tmp > waitingTime)  waitingTime=tmp;
		}
		return waitingTime;
	}
	
	/**
	 * Calculates the min waiting time for all the tasks.
	 */
	public static float getMinWaitingTime() {
		int waitingTime = taskQueue.peek().getFinishTime() - taskQueue.peek().getArrivalTime();
		for(Task t : taskQueue) {
			int tmp = t.getFinishTime() - t.getArrivalTime();
			if (tmp < waitingTime)  waitingTime=tmp;
		}
		return waitingTime;
	}

	public static int getSimulationFinishTime() {
		return simulationFinishTime;
	}

	public static void setSimulationFinishTime(int simualtionFinishTime) {
		Statistics.simulationFinishTime = simualtionFinishTime;
	}
}
