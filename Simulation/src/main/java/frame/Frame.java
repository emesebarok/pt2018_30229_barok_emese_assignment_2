package frame;

import model.*;
import controller.Statistics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Frame {

	JFrame frame = new JFrame("Queue Simulator");
	JPanel qPanel = new JPanel();
	JPanel btnPanel = new JPanel();
	JTextField txtSimInterval = new JTextField();
	JTextField txtMinArrivalInterval = new JTextField();
	JTextField txtMaxArrivalInterval = new JTextField();
	JTextField txtMinServiceTime = new JTextField();
	JTextField txtMaxServiceTime = new JTextField();
	JTextField txtMaxNumberOfSchedulers = new JTextField();
	JButton btnStart = new JButton("Start Simulation");
	JLabel lblCurrentTime = new JLabel("0");

	public Frame() {

		frame.setSize(900, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());

		btnPanel.setLayout(new GridLayout(1, 6));

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(2, 1));
		p1.add(new JLabel("Simulation Interval:"));
		p1.add(txtSimInterval);
		btnPanel.add(p1);
		
		JPanel p7 = new JPanel();
		p7.setLayout(new GridLayout(2, 1));
		p7.add(new JLabel("Max Nr of Queues:"));
		p7.add(txtMaxNumberOfSchedulers);
		btnPanel.add(p7);

		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(2, 1));
		p2.add(new JLabel("Min Arrival Interval:"));
		p2.add(txtMinArrivalInterval);
		btnPanel.add(p2);

		JPanel p3 = new JPanel();
		p3.setLayout(new GridLayout(2, 1));
		p3.add(new JLabel("Max Arrival Interval:"));
		p3.add(txtMaxArrivalInterval);
		btnPanel.add(p3);

		JPanel p4 = new JPanel();
		p4.setLayout(new GridLayout(2, 1));
		p4.add(new JLabel("Min Service Time:"));
		p4.add(txtMinServiceTime);
		btnPanel.add(p4);

		JPanel p5 = new JPanel();
		p5.setLayout(new GridLayout(2, 1));
		p5.add(new JLabel("Max Service Time:"));
		p5.add(txtMaxServiceTime);
		btnPanel.add(p5);

		JPanel p6 = new JPanel();
		p6.setLayout(new GridLayout(2, 1));
		lblCurrentTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblCurrentTime.setVerticalAlignment(SwingConstants.CENTER);
		p6.add(lblCurrentTime);
		p6.add(btnStart);
		btnPanel.add(p6);

		btnPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
		frame.add(btnPanel, BorderLayout.NORTH);

		qPanel.setLayout(new GridLayout(1, 3));
		frame.add(qPanel, BorderLayout.CENTER);

		frame.setVisible(true);
	}

	public void displayData(ArrayList<Scheduler> schedulers) {
		qPanel.removeAll();
		qPanel.revalidate();

		for (Scheduler scheduler : schedulers) {
			JPanel p = new JPanel();
			p.setLayout(new BorderLayout());
			if (scheduler.getServer().getCurrentTask() != null) {
				p.add(new JLabel("Current: " + scheduler.getServer().getCurrentTask().toString()), BorderLayout.NORTH);
			}
			p.add(new JLabel("Waiting time: " + scheduler.getServer().getWaitingTime()), BorderLayout.SOUTH);
			if (!scheduler.getServer().isEmpty()) {
				JList<Task> jtasks = new JList<Task>(scheduler.getServer().getTasks());
				JScrollPane sp = new JScrollPane(jtasks);
				p.add(sp, BorderLayout.CENTER);
			} else {
				if (scheduler.getServer().getCurrentTask() == null) {
					p.add(new JLabel("EMPTY QUEUE"));
				}
			}

			qPanel.add(p);

		}

		qPanel.repaint();
		qPanel.revalidate();

	}

	public void setStartButtonListener(ActionListener a) {
		btnStart.addActionListener(a);
	}

	public String getMinArrivalInterval() {
		return txtMinArrivalInterval.getText();
	}

	public String getMaxArrivalInterval() {
		return txtMaxArrivalInterval.getText();
	}

	public String getMinServiceTime() {
		return txtMinServiceTime.getText();
	}

	public String getMaxServiceTime() {
		return txtMaxServiceTime.getText();
	}

	public String getSimInterval() {
		return txtSimInterval.getText();
	}

	public String getMaxNumberOfSchedulers() {
		return txtMaxNumberOfSchedulers.getText();
	}

	public void setCurrentTime(int currentTime) {
		lblCurrentTime.setText(String.valueOf(currentTime));
	}

	public void displayErrorMessage(String s) {
		JOptionPane.showMessageDialog(frame, s + " is not in the correct format.", "Input Format Error",
				JOptionPane.ERROR_MESSAGE);

	}

	public void displayFinalStatistics() {
		JOptionPane.showMessageDialog(frame, "Min Waiting Time = " + Statistics.getMinWaitingTime() 
										   + " ; \nMax Waiting Time = " + Statistics.getMaxWaitingTime() 
										   + " ; \nAvg Waiting Time = " + Statistics.getAverageWaitingTime()
										   + " ; \nPeak Hour Start Time =  " + Statistics.getPeakHourStartingTime()
										   + " ; \nPeak Hour Task Number = " + Statistics.getPeakHourTaskNumber(),
										   "Final Statistics", JOptionPane.INFORMATION_MESSAGE);
	}
}
