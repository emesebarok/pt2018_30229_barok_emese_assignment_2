package model;

public class Scheduler {
	private Server server;
	private Thread th;
	
	/**
	 * Instantiates a scheduler objects and starts the server thread associated to it.
	 */
	public Scheduler(){
		server = new Server();
		th = new Thread(server);
		th.start();
	}
	
	/**
	 * Adds a task to the task queue of the server.
	 */
	public void dispatchTaskOnServer(Task t){
		server.addTask(t);
	}
	
	/**
	 * Gets the server associated to the scheduler.
	 */
	public Server getServer(){
		return server;
	}
	
	/**
	 * Sets the time variable of the Simulator with the Server.
	 */
	public void setCurrentTime(int currentTime) {
		server.setCurrentTime(currentTime);
	}
	
	/**
	 * Interrupts the thread on which the server is run.
	 */
	public void terminate() {
		th.interrupt();
	}
}
