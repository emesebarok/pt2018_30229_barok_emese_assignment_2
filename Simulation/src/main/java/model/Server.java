package model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import controller.Simulator;
import controller.Statistics;

public class Server implements Runnable {

	private static final int OVERLOAD_TRESHOLD = 10;
	private BlockingQueue<Task> taskQueue;
	private AtomicInteger waitingTime;
	private Task currentTask;
	private AtomicInteger currentTime;

	//Instantiates a server object initializing all of its attributes with 0 or empty.
	public Server() {
		waitingTime = new AtomicInteger(0);
		currentTime = new AtomicInteger(0);
		taskQueue = new LinkedBlockingQueue<>();
	}

	/** 
	 * Method containing the task processing code, it takes the head element from the task queue then goes into sleep for the service time of the
	 * given task and then puts it in the list of the finished tasks in the Logger class.
	 */
	public void run() {
		while (true) {
			try {
				if (taskQueue.isEmpty()) currentTask = null;
				currentTask = taskQueue.take();
				
				Thread.sleep(currentTask.getServiceTime() * Simulator.SLEEP_TIME_UNIT);
				waitingTime.addAndGet((-1) * currentTask.getServiceTime());
				
				if (currentTask != null) {
					currentTask.setFinishTime(currentTime.get());
					Statistics.taskQueue.add(currentTask);
					if (taskQueue.isEmpty()) currentTask = null;
				}
			} catch (InterruptedException e) {
				System.out.println("Thread terminated.");
			}
		}

	}

	/**
	 * Gets the current time from the simulator and synchronizes the current time of the thread with this value.
	 */
	public void setCurrentTime(int currentTime) {
		this.currentTime.set(currentTime);
	}

	/**
	 * Adds a task to the task queue
	 */
	public void addTask(Task t) {
		taskQueue.add(t);
		waitingTime.addAndGet(t.getServiceTime());

	}

	/**
	 * Removes a given task from the task queue.
	 */
	public void removeTask(Task t) {
		taskQueue.remove(t);
		waitingTime.addAndGet((-1) * t.getServiceTime());
	}

	/**
	 * Gets all the tasks in the form of an array form the task queue.
	 */
	public Task[] getTasks() {

		Task[] task = new Task[taskQueue.size()];
		taskQueue.toArray(task);

		return task;
	}

	/**
	 * Gets the task which is currently being processed / serviced.
	 */
	public Task getCurrentTask() {
		return this.currentTask;
	}

	/**
	 * Verifies if the task queue is empty.
	 */
	public boolean isEmpty() {
		return taskQueue.isEmpty();
	}

	/**
	 * Gets the total waiting time of the task queue.
	 */
	public int getWaitingTime() {
		return waitingTime.get();
	}

	/**
	 * Gets the size of the task queue.
	 */
	public int getSize() {
		return taskQueue.size();
	}

	/**
	 * It verifies if the number of tasks in the queue is larger than the OVERLOAD_TRESHOLD
	 */
	public boolean isOverloaded() {
		return taskQueue.size() > OVERLOAD_TRESHOLD;
	}

}
