package model;

public class Task {
	private int arrivalTime;
	private int serviceTime;
	private int finishTime = 0;
	private int q;
	
	//Instantiates a new task object specifying the arrivalTime and serviceTime. Initially finishTime is set to 0, which also signals that the given task
	public Task(int arrivalTime, int serviceTime, int q){
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
		this.q = q;
	}

	//Gets the arrival time of the current task.
	public int getArrivalTime() {
		return arrivalTime;
	}

	//Sets the arrival time of the current task.
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	//Gets the service time of the current task.
	public int getServiceTime() {
		return serviceTime;
	}

	//Sets the service time of the current task.
	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}
	
	//Gets the finish time of the current task.
	public int getFinishTime() {
		return finishTime;
	}
	
	//Sets the service time of the current task.
	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}
	
	//Gets the nr of queue of the current task.
	public int getQ() {
		return q;
	}
		
	//Sets the nr of queue of the current task.
	public void setQ(int q) {
		this.q = q;
	}

	public String toString(){
		if (finishTime == 0) 
			return "Arrived: " + String.valueOf(arrivalTime)+ " Service Time: " + String.valueOf(serviceTime) + " In line: " + String.valueOf(q);
		else
			return "Arrived: " + String.valueOf(arrivalTime)+ " Service Time: " + String.valueOf(serviceTime) + " Finished: " + String.valueOf(finishTime)  + " In line: " + String.valueOf(q);
	}

}
